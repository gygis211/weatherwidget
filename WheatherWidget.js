
fetch("http://api.openweathermap.org/data/2.5/weather?lang=ru&q=minsk&appid=ccd5d27dec4aa937b1615c9650c1046e")
    .then(function (resp) { return resp.json() })
    .then(function (data) {
        console.log(data)
        document.querySelector('.city-name').textContent = data.name;
        document.querySelector('.temperature').innerHTML = "Температура: " + Math.round((data.main.temp) - 273.15) + '°C';
        document.querySelector('.temperature-feel').innerHTML = "По ощущению: " + Math.round((data.main.feels_like) - 273.15) + '°C';
        document.querySelector('.obtrusiveness').textContent = data.weather[0]['description'];
        document.querySelector('.wind-speed').textContent = "Cкорость ветра: " + data.wind.speed + "м/с";
        document.querySelector('.icon').innerHTML = `<img src=http://openweathermap.org/img/wn/${data.weather[0].icon}.png>`;
    })
    .catch(function (error) {
        error
    });

fetch("http://api.openweathermap.org/data/2.5/forecast?lang=ru&q=minsk&appid=ccd5d27dec4aa937b1615c9650c1046e")
    .then(function (resp) { return resp.json() })
    .then(function (data) {

        const { list } = data;
        let weatherForThreeDays = [];
        list.slice(0, 24).map(item => {
            if (item.dt_txt.includes('12:00:00')) {
                weatherForThreeDays.push(item);
            }
        })
        console.log(weatherForThreeDays)

        document.querySelector('.city-name1').innerHTML = weatherForThreeDays[0].dt_txt;
        document.querySelector('.temperature1').innerHTML = "Температура: " + Math.round((weatherForThreeDays[0].main.temp) - 273.15) + '°C';
        document.querySelector('.temperature-feel1').innerHTML = "По ощущению: " + Math.round((weatherForThreeDays[0].main.feels_like) - 273.15) + '°C';
        document.querySelector('.obtrusiveness1').textContent = weatherForThreeDays[0].weather[0]['description'];
        document.querySelector('.wind-speed1').textContent = "Cкорость ветра: " + weatherForThreeDays[0].wind.speed + "м/с";
        document.querySelector('.icon1').innerHTML = `<img src=http://openweathermap.org/img/wn/${weatherForThreeDays[0].weather[0].icon}.png>`;

        document.querySelector('.city-name2').innerHTML = weatherForThreeDays[1].dt_txt;
        document.querySelector('.temperature2').innerHTML = "Температура: " + Math.round((weatherForThreeDays[1].main.temp) - 273.15) + '°C';
        document.querySelector('.temperature-feel2').innerHTML = "По ощущению: " + Math.round((weatherForThreeDays[1].main.feels_like) - 273.15) + '°C';
        document.querySelector('.obtrusiveness2').textContent = weatherForThreeDays[1].weather[0]['description'];
        document.querySelector('.wind-speed2').textContent = "Cкорость ветра: " + weatherForThreeDays[1].wind.speed + "м/с";
        document.querySelector('.icon2').innerHTML = `<img src=http://openweathermap.org/img/wn/${weatherForThreeDays[1].weather[0].icon}.png>`;

        document.querySelector('.city-name3').innerHTML = weatherForThreeDays[2].dt_txt;
        document.querySelector('.temperature3').innerHTML = "Температура: " + Math.round((weatherForThreeDays[2].main.temp) - 273.15) + '°C';
        document.querySelector('.temperature-feel3').innerHTML = "По ощущению: " + Math.round((weatherForThreeDays[1].main.feels_like) - 273.15) + '°C';
        document.querySelector('.obtrusiveness3').textContent = weatherForThreeDays[2].weather[0]['description'];
        document.querySelector('.wind-speed3').textContent = "Cкорость ветра: " + weatherForThreeDays[2].wind.speed + "м/с";
        document.querySelector('.icon3').innerHTML = `<img src=http://openweathermap.org/img/wn/${weatherForThreeDays[2].weather[0].icon}.png>`;
    })
    .catch(function (error) {
        error
    });

let buttonOpen = document.querySelector('#buttonOpen'),
    buttonClose = document.querySelector('#buttonClose'),
    containerThreeDays = document.querySelector('#threeDays');

    buttonOpen.onclick = () => {
        containerThreeDays.classList.remove('containerThreeDaysClose');
        containerThreeDays.classList.add('containerThreeDays');
    };
    buttonClose.onclick = () => {
        containerThreeDays.classList.remove('containerThreeDays');
        containerThreeDays.classList.add('containerThreeDaysClose');
    };